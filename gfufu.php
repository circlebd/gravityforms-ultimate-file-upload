<?php


// Save image to the media library after form submission
add_action( 'gform_after_submission', 'access_entry_via_field', 10, 2 );
function access_entry_via_field( $entry, $form ) {
    foreach ( $form[ 'fields' ] as $field ) {
		if( isset( $field[ 'cssClass' ] ) && !empty( $field[ 'cssClass' ] ) && $field[ 'cssClass' ] == 'media_file' ):
			$img_url = $entry[ $field[ 'id' ] ];

			$filetype = wp_check_filetype( basename( $img_url ), null );
			if( $filetype && isset( $filetype['ext'] ) && in_array( $filetype['ext'], array( 'jpg', 'png', 'jpeg', 'gif', 'ico' ) ) ){
				$attach_id =  isset( $img_url ) && !empty( $img_url ) ? gform_insert_media_image( $img_url ) : 0;
			}else{
				$attach_id =  isset( $img_url ) && !empty( $img_url ) ? gform_insert_media_file( $img_url ) : 0;
			}
			 

			// Save the update
			if( $attach_id ) {
				//gform_update_meta( $entry[ 'id' ], $field[ 'id' ], $attach_id );
				gform_update_meta( $entry[ 'id' ], 'imgID_'.$field[ 'id' ], $attach_id );
				gform_update_meta( $entry[ 'id' ], 'media_file_'.$field[ 'id' ], true );
			}
			
		endif;
    }
}

// Show media image to the entry detail
add_filter( 'gform_entry_field_value', 'category_names', 10, 4 );
function category_names( $value, $field, $lead, $form ) {
	if( isset( $field[ 'cssClass' ] ) && !empty( $field[ 'cssClass' ] ) && $field[ 'cssClass' ] == 'media_file' ):
		$image_status = gform_get_meta( $lead[ 'id' ], 'media_file_'.$field[ 'id' ] );
		
		if ( !$image_status ):
			// Save image as attachment
			$img_url = $lead[ $field[ 'id' ] ];			
			$filetype = wp_check_filetype( basename( $img_url ), null );
			if( $filetype && isset( $filetype['ext'] ) && in_array( $filetype['ext'], array( 'jpg', 'png', 'jpeg', 'gif', 'ico' ) ) ){
				$attach_id =  isset( $img_url ) && !empty( $img_url ) ? gform_insert_media_image( $img_url ) : 0;
			}else{
				$attach_id =  isset( $img_url ) && !empty( $img_url ) ? gform_insert_media_file( $img_url ) : 0;
			} 

			// Save the update
			if( $attach_id ) {
				//gform_update_meta( $lead[ 'id' ], $field[ 'id' ], $attach_id );
				gform_update_meta( $lead[ 'id' ], 'imgID_'.$field[ 'id' ], $attach_id );
				gform_update_meta( $lead[ 'id' ], 'media_file_'.$field[ 'id' ], true );
				$image_status = true;
			}
		endif;

		if ( $image_status ):
			$image_id = gform_get_meta( $lead[ 'id' ], 'imgID_'.$field[ 'id' ] );
			$image_title = get_the_title( $image_id );
			$file_type = get_icon_for_attachment( $image_id ) ? get_icon_for_attachment( $image_id ) : '';
			$file_url = !empty( $file_type ) && $file_type == 'icon_image' ? wp_get_attachment_url( $image_id ) : plugin_dir_url( __FILE__ ).'assets/img/file.png'; 
			$value = '<a href="'.get_edit_post_link( $image_id ).'" style="padding:5px; display: block; width: max-content;" target="_blank">
						<img height="50" src="'.$file_url.'" /><br><span>'.$image_title.'</span></a>'; 
		endif;
		
	endif;
	return $value;
}

// Show media image to the entry list
add_filter( 'gform_entries_column_filter', 'change_column_data', 10, 5 );
function change_column_data( $value, $form_id, $field_id, $entry ) {
	$field = RGFormsModel::get_field( $form_id, $field_id );
	if( !empty( $field->cssClass ) && $field->cssClass == 'media_file' ):
		$media_file = gform_get_meta( $entry[ 'id' ], 'media_file_'.$field_id );
		$image_id = gform_get_meta( $entry[ 'id' ], 'imgID_'.$field[ 'id' ] );
		if( $media_file && $image_id  ){
			$file_type = get_icon_for_attachment( $image_id ) ? get_icon_for_attachment( $image_id ) : 'icon_generic';
			$value = '<a href="'.get_edit_post_link( $image_id ).'" target="_blank" aria-label="View the image"><img src="'.plugin_dir_url( __FILE__ ).'assets/img/doctypes/'.$file_type.'.gif" alt=""></a>';
			return $value;
		}
	endif;
    return $value;
}

// Insert media image function
function gform_insert_media_image( $img_url ){
	
	// Gives us access to the download_url() and wp_handle_sideload() functions
	//$filetype = wp_check_filetype( basename( $img_url ), null );

	$filename = pathinfo( $img_url, PATHINFO_FILENAME );
	include_once( ABSPATH . 'wp-admin/includes/image.php' );
	include_once( ABSPATH . 'wp-admin/includes/file.php' );
	include_once( ABSPATH . 'wp-admin/includes/media.php' );

	$new_url = stripslashes( $img_url );
	$result = media_sideload_image( $new_url, 0, $filename, 'id');
	//saving the image to field or thumbnail

	return $result;

}

// Insert media file function
function gform_insert_media_file( $img_url ){
	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype( basename( $img_url ), null );
			
	// Get the path to the upload directory.
	$wp_upload_dir = wp_upload_dir();
	
	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $img_url ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $img_url ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);
	
	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $img_url );
	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );
	
	// Generate the metadata for the attachment, and update the database record.
	$attach_data = wp_generate_attachment_metadata( $attach_id, $img_url );
	wp_update_attachment_metadata( $attach_id, $attach_data );

	return $attach_id;
}

//GET Application Mime Type
function get_icon_for_attachment($post_id) {
	$type = get_post_mime_type($post_id) ? get_post_mime_type($post_id) : '';
	switch ($type) {
		case 'image/jpeg':
		case 'image/png':
		case 'image/gif':
		case 'image/x-icon':
			return "icon_image"; break;
		case 'application/msword':
		case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
			return "icon_doc" ; break;
		case 'application/pdf':
				return "icon_pdf" ; break;	
		case 'video/mpeg':
		case 'video/mp4': 
		case 'video/quicktime':
			return "icon_video"; break;
		case 'text/csv':
		case 'text/plain': 
		case 'text/xml':
			return "icon_txt"; break;
		default:
			return "icon_generic"; break;
	}
}
