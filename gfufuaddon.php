<?php


if( !class_exists( 'GFUFUAddon' ) ) {

    class GFUFUAddon{
        protected $_has_tag_inputs = false;
        protected $_included_js;
        protected $_tag_inputs = array();
        protected $_tag_map = array();
        protected $_tag_terms = array();

        public function init() {
            add_action( 'gform_field_advanced_settings', array( $this, 'gfufu_media_library_settings' ), 10, 2 );
            add_action( 'gform_editor_js', array( $this, 'gufu_save_in_media_library' ) );
            add_filter( 'gform_tooltips', array( $this, 'gfufu_media_library_tooltips' ) );
        }

        function gfufu_media_library_settings( $position, $form_id ) {
            if ( $position == 100 ) {
                ?>
                <li class="media_library_field_setting field_setting">
                    <label for="field_media_library" class="section_label">
                        <?php _e("Save in media library", "gravityforms"); ?>
                        <?php gform_tooltip("form_field_media_library") ?>
                    </label>
                    <input type="checkbox" id="field_media_library" onclick="SetFieldProperty('mediaLibrary', this.checked);" /><label for="field_media_library" class="inline"> Enable</lebel>
                </li>
                <?php
            }
        }

        function gufu_save_in_media_library(){
            ?>
            <script type='text/javascript'>
                //adding setting to fields of type "text"
                fieldSettings.fileupload += ", .media_library_field_setting";
         
                //binding to the load field settings event to initialize the checkbox
                jQuery(document).on("gform_load_field_settings", function(event, field, form){
                    jQuery("#field_media_library").attr("checked", field["mediaLibrary"] == true);
                });
            </script>
            <?php
        }

        function gfufu_media_library_tooltips( $tooltips ) {
            $tooltips['form_field_media_library'] = "<h6>Media Libarry</h6>Check this box to save image to media library.";
            return $tooltips;
        }

    }
}

add_action( 'gform_field_advanced_settings', 'gfufu_media_library_settings', 10, 2 );
function gfufu_media_library_settings( $position, $form_id ) {
    if ( $position == 100 ) {
        ?>
        <li class="media_library_field_setting field_setting">
            <label for="field_media_library" class="section_label">
                <?php _e("Save in media library", "gravityforms"); ?>
                <?php gform_tooltip("form_field_media_library") ?>
            </label>
            <input type="checkbox" id="field_media_library" onclick="SetFieldProperty('mediaLibrary', this.checked);" /><label for="field_media_library" class="inline"> Enable</lebel>
        </li>
        <?php
    }
}
 
//Action to inject supporting script to the form editor page
add_action( 'gform_editor_js', 'gufu_save_in_media_library' );
function gufu_save_in_media_library(){
    ?>
    <script type='text/javascript'>
        //adding setting to fields of type "text"
        fieldSettings.fileupload += ", .media_library_field_setting";
 
        //binding to the load field settings event to initialize the checkbox
        jQuery(document).on("gform_load_field_settings", function(event, field, form){
            jQuery("#field_media_library").attr("checked", field["mediaLibrary"] == true);
        });
    </script>
    <?php
}
 
//Filter to add a new tooltip
add_filter( 'gform_tooltips', 'gfufu_media_library_tooltips' );
function gfufu_media_library_tooltips( $tooltips ) {
   $tooltips['form_field_media_library'] = "<h6>Media Libarry</h6>Check this box to save image to media library.";
   return $tooltips;
}


// SAVE FILES TO MEDIA LIBRARY

add_action( 'gform_after_submission', 'gfufu_set_post_content', 10, 2 );
function gfufu_set_post_content( $entry, $form ) {
    foreach( $form[ 'fields' ] as $field ){
        echo '<pre>';
        //print_r( $field );
        echo '</pre>';
        $post	= get_post( $entry['post_id'] );
        if( ( isset( $field[ 'type' ] ) && $field[ 'type' ] == 'fileupload' ) || ( isset( $field[ 'inputType' ] ) && $field[ 'inputType' ] == 'fileupload' ) ){ // need function to determine this
            if( isset( $field[ 'mediaLibrary' ] ) && $field[ 'mediaLibrary' ] == true ){
                if( isset( $field[ 'multipleFiles' ] ) && $field[ 'multipleFiles' ] == true ){
                    $images = array();
                    $values = rgar( $entry, (string) $field->id );
                    $values = preg_replace( '/[\[\]"]/', '', $values );
                    $values = explode(',', $values);
                    if( $values && !empty( $values ) ):
                        foreach( $values as $key=>$value):
                            $value = stripslashes( $value );
                            $images[] = GFFormsModel::media_handle_upload( $value, $post->ID );
                            //if( $key == 0 && !empty( $value ) && $post  ) set_post_thumbnail( $post->ID, $images[$key] );
                        endforeach;
                    endif;
                    //if( $post && $images && !empty( $images ) ) update_post_meta( $post->ID, 'celebration_gallery', $images );
                }else{
                    $value = rgar( $entry, (string) $field->id );
                    if( $value && !empty( $value ) ):
                        $value = stripslashes( $value );
                        $image = GFFormsModel::media_handle_upload( $value, $post->ID );
                    endif;
                }             
            } 
        }
    }
}