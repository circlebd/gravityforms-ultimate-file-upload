<?php
/**
* Plugin Name: Gravity Forms Ultimate File Upload
* Plugin URI:  http://wphacks.xyz
* Description: Gravity form ultimate file upload plugin allows user to use gravity from file upload in an extra mile. It allows user to add media upload to upload as attachment and shows in media library, well designed draggable regular and multiple file file upload to posts and many more. 
* Version:     1.0.0
* Author:      wphacks_mahfuz
* Author URI:  http://wphacks.xyz/mahfuz
* Donate link: http://wphacks.xyz
* License:     GPLv2+
*/

/**
* Copyright (c) 2018 webpublisherpro (email : support@webpublisherpro.com)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License, version 2 or, at
* your discretion, any later version, as published by the Free
* Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

add_action('init',  array('GFUFUAddon', 'init'), 20);
add_action('admin_notices', array('GFUFUAddon', 'admin_warnings'), 20);


class GFUFUAddon
{
	private static $name = 'Gravity Forms Ultimate File Upload';
    private static $slug = 'GFUFUAddon';
    private static $version = '1.0.0';
	private static $min_gravityforms_version = '1.9.3';
	
	//Plugin starting point. Will load appropriate files
    public static function init(){
		if( self::is_gravityforms_installed() ):
			//include the base class
            require_once(self::get_base_path() . '/gfufuaddon.php');
		endif;
	}

	//display admin warnings if GF is not the correct version or GF is not installed
    public static function admin_warnings() {
        if ( !self::is_gravityforms_installed() ) {
            $message = __('requires Gravity Forms to be installed.', self::$slug);
        } else if ( !self::is_gravityforms_supported() ) {
            $message = __('requires a minimum Gravity Forms version of ', self::$slug) . self::$min_gravityforms_version;
        }

        if (empty($message)) {
            return;
        }
        ?>
        <div class="error">
            <p>
                <?php _e('The plugin ', self::$slug); ?><strong><?php echo self::$name; ?></strong> <?php echo $message; ?><br />
                <?php _e('Please ',self::$slug); ?><a href="http://bit.ly/getgravityforms"><?php _e(' download the latest version ',self::$slug); ?></a><?php _e(' of Gravity Forms and try again.',self::$slug) ?>
            </p>
        </div>
        <?php
    }

	/*
     * Check if GF is installed
     */
    private static function is_gravityforms_installed(){
        return class_exists( 'RGForms' );
	}
	
	/*
     * Check if the installed version of GF is supported
     */
    private static function is_gravityforms_supported(){
        return self::check_gravityforms_version( self::$min_gravityforms_version,'>=' );
    }

    /*
     * Do a GF version compare
     */
    private static function check_gravityforms_version($version, $operator){
        if(class_exists('GFCommon')){
            return version_compare( GFCommon::$version, $version, $operator );
        }
        return false;
    }

    /*
     * Returns the url of the plugin's root folder
     */
    protected function get_base_url(){
        return plugins_url(null, __FILE__);
    }

    /*
     * Returns the physical path of the plugin's root folder
     */
    protected static function get_base_path(){
        return dirname( __FILE__ );
    }

}